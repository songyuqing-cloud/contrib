# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.3] - 2020-Dec-15
### Changed
- Set build dep bindgen to 0.56

## [0.5.2] - 2020-08-24
### Changed
- Set build dep bindgen to 0.55.0
- Set build dep pkgconf to 0.3.18

## [0.5.1] - 2020-07-14
### Changed
- Set build dep bindgen to 0.54.3
- Set build dep pkgconf to 0.3.14

## [0.4.0] - 2020-03-25
### Changed
- Set minimum requirement for librealsense2 to version 2.33.1 as it fixes issues with timestamps.

## [0.3.0] - 2020-02-20
### Added
- Rustification of `rs2_distortion` enum.

## [0.2.0]
### Added
- Prior to 0.3.0 this repository did not have a changelog.
