# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.6] - 2020-07-31
### Changed
- Bump cargo.toml dep on bindgen to 0.55.0 due to yanked 0.54.1

## [0.1.5] - 2020-07-31
### Changed
- Bump cargo.toml dep on bindgen to 0.54.1

## [0.1.4] - 2020-05-26
### Changed
- Moved repo to public subgroup
- Added MIT license

## [0.1.3] - 2020-01-28
### Added
- Support for linking with `pkgconfig`.


## [0.1.2] - 2019-12-04
### Added
- Compiler linking of `k4arecord` library.


## [0.1.1] - 2019-11-13
### Added
- Add public access to k4a C enumerations under Rustified names. E.g. `k4a_image_format_t` as `ImageFormat`.
- Add `README.md` and this `CHANGELOG.md`.
### Modified
- Fixed compiler linking of `k4a` library.


## [0.1.0]
### Added
- Prior to 0.1.1 this repository did not have a changelog.
